package com.example.seok.tourism.DB_Request;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jung Se In on 2018-01-24.
 */

public class RegistRequest extends StringRequest {

    final static private String URL = "http://203.241.246.138/Medical/Client/Register_tour.php";

    private Map<String, String> parameters;

    public RegistRequest(String user_id,String user_pw,String user_name,int user_age,String user_gender,String user_adress, String user_tel, Response.Listener<String> Listener) {
        super(Method.POST, URL, Listener, null);
        parameters = new HashMap<>();
        parameters.put("user_id",user_id);
        parameters.put("user_pw",user_pw);
        parameters.put("user_name",user_name);
        parameters.put("user_age",user_age+"");
        parameters.put("user_gender",user_gender);
        parameters.put("user_adress",user_adress);
        parameters.put("user_tel",user_tel);
    }

    @Override
    public Map<String, String> getParams(){    return parameters;}
}

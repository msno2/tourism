package com.example.seok.tourism;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.seok.tourism.DB_Request.LoginRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {

    private EditText textID, textPW;
    private Button btnLogin;
    private TextView textRegister, textFind;
    private CheckBox checkBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        boolean isWifiAvail = ni.isAvailable();
        boolean isWifiConn = ni.isConnected();
        ni = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        boolean isMobileAvail = ni.isAvailable();
        boolean isMobileConn = ni.isConnected();

        if(isWifiConn==false && isMobileConn==false)
        {
            AlertDialog.Builder alert_internet_status = new AlertDialog.Builder(this);
            alert_internet_status.setTitle( "인터넷연결" );
            alert_internet_status.setMessage( "인터넷이 연결되지 않았습니다. 연결을 확인하세요" );
            alert_internet_status.setPositiveButton( "닫기", new DialogInterface.OnClickListener() {
                public void onClick( DialogInterface dialog, int which) {
                    dialog.dismiss();   //닫기
                }
            });
            alert_internet_status.show();
        }

        textID = (EditText)findViewById(R.id.textID);
        textPW = (EditText)findViewById(R.id.textPW);

        checkBox = (CheckBox)findViewById(R.id.checkBoxL);

        textRegister = (TextView)findViewById(R.id.textRegister);
        textRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, Register_id_psw_Activity.class));
            }
        });

        textFind = (TextView)findViewById(R.id.textFind);
        textFind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(LoginActivity.this,"아이디/비밀번호 찾기 페이지 연결.",Toast.LENGTH_SHORT).show();
            }
        });

        btnLogin = (Button)findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String user_id = textID.getText().toString();
                final String user_pw = textPW.getText().toString();
                Log.d("LoginEx",user_id+"< "+ user_pw+"< ");
                Response.Listener<String> responseListener = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("LoginEx", "success");
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");
                            Log.d("LoginEx", success+"");
                            if (success) {
                                inputUserInfo(jsonResponse);
                                String user_id = jsonResponse.getString("user_id");

                                //String userPassword = jsonResponse.getString("userPassword"); 정보 얻어올때
                                int term = jsonResponse.getInt("term");
                                if(term == 1)
                                {
                                    Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                                else
                                {
                                    Intent intent = new Intent(LoginActivity.this,TermsActivity.class);
                                    intent.putExtra("user_id",user_id);
                                    Log.d("aa",user_id+"");
                                    startActivity(intent);
                                    finish();
                                }
                            }
                            else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                                builder.setMessage("로그인 실패")
                                        .setNegativeButton("다시 시도", null)
                                        .create()
                                        .show();
                            }
                        } catch (Exception e) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                            builder.setMessage("접속실패 (DB ERROR)")
                                    .setNegativeButton("다시 시도", null)
                                    .create()
                                    .show();
                            e.printStackTrace();
                        }
                    }
                };

                LoginRequest loginRequest = new LoginRequest(user_id, user_pw,responseListener);
                RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);
                queue.add(loginRequest);
            }
        });
    }
    public void inputUserInfo(JSONObject jsonResponse){
        try {
            UserInfomation.getInstance().setUserId(jsonResponse.getString("user_id"));
            UserInfomation.getInstance().setUserName(jsonResponse.getString("user_name"));
            UserInfomation.getInstance().setUserTel(jsonResponse.getString("user_tel"));
            UserInfomation.getInstance().setUserAge(jsonResponse.getString("user_age"));
            UserInfomation.getInstance().setUserGender(jsonResponse.getString("user_gender"));
            UserInfomation.getInstance().setUserAddress(jsonResponse.getString("user_address"));
            UserInfomation.getInstance().setUserConsulting(jsonResponse.getString("consulting"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}

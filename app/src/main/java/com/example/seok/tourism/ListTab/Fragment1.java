package com.example.seok.tourism.ListTab;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.seok.tourism.ExplainActivity;
import com.example.seok.tourism.R;

import java.util.ArrayList;

public class Fragment1 extends Fragment implements View.OnClickListener {

    private View view;
    private Spinner spinner;
    private listMenu listMenu;
    private ListView listViewTour;
    private static String str1 = "";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.activity_fragment1, container,false);

        int[] strImg = {R.drawable.testtour, R.drawable.testtour2,R.drawable.testtour3,R.drawable.testtour4};
        String[]  strName= {"여행정보1-","여행정보2-","여행정보3-","여행정보4-"};
        int[] strid = {1,2,3,4};
        int nDatCnt = 0;

        ArrayList<listMenu> arrayList = new ArrayList<>();

        for(int i = 0; i<strName.length; i++) {
            listMenu = new listMenu();
            listMenu.tourName = strName[nDatCnt];
            listMenu.tourImg = strImg[nDatCnt];
            listMenu.tourid = strid[nDatCnt];
            Log.d("aaa",listMenu.tourid+"");
            listMenu.OnclickListener = this;
            nDatCnt++;
            // 디비의 순서대로
            arrayList.add(listMenu);
            if (nDatCnt >= strName.length)
                nDatCnt = 0;
        }

        listViewTour = (ListView)view.findViewById(R.id.listview_tour);
        menuAdapter adapter = new menuAdapter(arrayList);
        listViewTour.setAdapter(adapter);


        spinner = (Spinner)view.findViewById(R.id.spinner_tour);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                //DB작업.
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
        return view;
    }

    @Override
    public void onClick(View v) {
        int nViewTag = Integer.parseInt((String)v.getTag());

        View oParentView = (View)v.getParent(); // 부모의 View를 가져온다. 즉, 아이템 View임.

        switch (nViewTag)
        {
            case 1:
                String position = (String) oParentView.getTag();

                str1 = "여행 : " + position + "입니다.";
                break;
            case 2:
                Intent intent = new Intent(getActivity(),ExplainActivity.class);
                startActivity(intent);
                break;
        }
    }
}

class menuAdapter extends BaseAdapter {
    LayoutInflater inflater = null;
    private ArrayList<listMenu> m_oData = null;
    private int nListCnt = 0;
    public menuAdapter(ArrayList<listMenu> _oData)
    {
        m_oData = _oData;
        nListCnt = m_oData.size();
    }
    @Override
    public int getCount() {
        Log.d("TAG", "getCount");
        return nListCnt;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            final Context context = parent.getContext();
            if (inflater == null) {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }
            convertView = inflater.inflate(R.layout.llistview_tour, parent, false);
        }

        LinearLayout tourLayout = (LinearLayout)convertView.findViewById(R.id.tourImg);
        TextView textMenuName1 = (TextView) convertView.findViewById(R.id.textview_tourlist);
        textMenuName1.setText(m_oData.get(position).tourName);

        Button btntour = (Button)convertView.findViewById(R.id.btn_tourlist);
        btntour.setTag("1");

        tourLayout.setBackgroundResource(m_oData.get(position).tourImg);
        tourLayout.setTag("2");

        tourLayout.setOnClickListener(m_oData.get(position).OnclickListener);
        btntour.setOnClickListener(m_oData.get(position).OnclickListener);


        convertView.setTag(""+position);
        return convertView;
    }
}

class listMenu{
    public int tourImg;
    public String tourName;
    public int tourid;
    public View.OnClickListener OnclickListener;
}
package com.example.seok.tourism;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.seok.tourism.DB_Request.LoginRequest;
import com.example.seok.tourism.DB_Request.TermRequest;

import org.json.JSONObject;

public class TermsActivity extends AppCompatActivity {

    private TextView textLogin;
    private CheckBox checkBox;
    private String user_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);

        Intent intent = getIntent();
        user_id = intent.getStringExtra("user_id");
        Log.d("aaa",user_id+"");
        checkBox = (CheckBox)findViewById(R.id.checkBox);
        textLogin = (TextView)findViewById(R.id.textLogin);

        textLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkBox.isChecked())
                {
                    Response.Listener<String> responseListener = new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonResponse = new JSONObject(response);
                                boolean success = jsonResponse.getBoolean("success");
                                if(success)
                                {
                                    startActivity(new Intent(TermsActivity.this, MainActivity.class));
                                    finish();
                                }
                                else
                                {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(TermsActivity.this);
                                    builder.setMessage("로그인 실패")
                                            .setNegativeButton("다시 시도", null)
                                            .create()
                                            .show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    };

                    TermRequest termRequest = new TermRequest(user_id,responseListener);
                    RequestQueue queue = Volley.newRequestQueue(TermsActivity.this);
                    queue.add(termRequest);
                }
                else
                {
                    Toast.makeText(TermsActivity.this,"약관의 동의가 필요합니다.",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}

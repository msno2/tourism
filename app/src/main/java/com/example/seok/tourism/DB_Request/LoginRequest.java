package com.example.seok.tourism.DB_Request;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jung Se In on 2018-01-24.
 */

public class LoginRequest extends StringRequest {

    final static private String URL = "http://203.241.246.138/Medical/Client/Login_tour.php";


    private Map<String, String> parameters;

    public LoginRequest(String user_id, String user_pw,Response.Listener<String> Listener) {
        super(Method.POST, URL, Listener, null);
        parameters = new HashMap<>();
        parameters.put("user_id",user_id);
        parameters.put("user_pw",user_pw);
    }

    @Override
    public Map<String, String> getParams(){    return parameters;}
}

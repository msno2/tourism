package com.example.seok.tourism.DB_Request;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jung Se In on 2018-03-13.
 */
public class ConsultingRequest_MyPackage extends StringRequest {
    final static private String URL = "http://203.241.246.138/Medical/Client/consult_stanby.php";

    private Map<String, String> parameters;

    public ConsultingRequest_MyPackage(String user_id, Response.Listener<String> Listener) {
        super(Method.POST, URL, Listener, null);
        parameters = new HashMap<>();
        parameters.put("user_id",user_id);
    }

    @Override
    public Map<String, String> getParams(){    return parameters;}
}
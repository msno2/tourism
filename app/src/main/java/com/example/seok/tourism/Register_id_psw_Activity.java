package com.example.seok.tourism;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.seok.tourism.DB_Request.IDcheckRequest;
import com.example.seok.tourism.DB_Request.LoginRequest;

import org.json.JSONObject;

import java.io.IOException;

public class Register_id_psw_Activity extends AppCompatActivity {

    private Button btnIDCheck, btnNext,btnPWCheck;
    private EditText textID,textPW,textPWT;
    private boolean IDcheck, PWcheck;
    private MediaPlayer mediaPlayer = new MediaPlayer();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_id_psw_);

        textID = (EditText)findViewById(R.id.textID);
        textPW = (EditText)findViewById(R.id.textpw);
        textPWT = (EditText)findViewById(R.id.textpwt);
        IDcheck =false; //test후 바꿔야됨
        PWcheck = false;
        btnIDCheck = (Button)findViewById(R.id.btnidcheck);
        btnIDCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String strID = textID.getText().toString();
                if(strID.equals("")){
                    Toast.makeText(Register_id_psw_Activity.this, "아이디칸이 비었습니다.", Toast.LENGTH_SHORT).show();
                    /*try {
                        mediaPlayer.setDataSource("http://www.imssong.com/sswv2/songaudio//0816ded305761d09154cec7458ededaf12f8b77c29ada56bfcdcd2da4bdd45a6.mp3");
                        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer mp) {
                                mediaPlayer.start();
                            }
                        });
                        mediaPlayer.prepareAsync();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }*/
                }
                else
                {
                    Log.d("aaa", "111" + strID);
                    Response.Listener<String> responseListener = new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                Log.d("aaa", "1");
                                JSONObject jsonResponse = new JSONObject(response);
                                Log.d("aaa", "2");
                                boolean success = jsonResponse.getBoolean("success");
                                Log.d("aaa", "" + success);
                                if (!success) {
                                    Log.d("aaa", "3");
                                    AlertDialog.Builder builder = new AlertDialog.Builder(Register_id_psw_Activity.this);
                                    builder.setMessage("사용 가능한 아이디 입니다.")
                                            .setPositiveButton("확인", null)
                                            .create()
                                            .show();
                                    IDcheck = true;
                                } else {
                                    Log.d("aaa", "4");
                                    AlertDialog.Builder builder = new AlertDialog.Builder(Register_id_psw_Activity.this);
                                    builder.setMessage("중복된 아이디가 있습니다.")
                                            .setNegativeButton("다시 시도", null)
                                            .create()
                                            .show();
                                    IDcheck = false;
                                }
                            } catch (Exception e) {
                                Log.d("aaa", "5");
                                Log.d("aaa", e.toString());
                                e.printStackTrace();
                            }
                        }
                    };

                    IDcheckRequest iDcheckRequest = new IDcheckRequest(strID, responseListener);
                    RequestQueue queue = Volley.newRequestQueue(Register_id_psw_Activity.this);
                    queue.add(iDcheckRequest);
                }
            }
        });



        btnNext = (Button)findViewById(R.id.btnnext);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (IDcheck == true && PWcheck == true) {
                    Intent intent = new Intent(Register_id_psw_Activity.this, RegisterActivity.class);
                    intent.putExtra("user_id",textID.getText().toString());
                    intent.putExtra("user_pw",textPW.getText().toString());
                    startActivity(intent);
                    finish();
                }
                else
                {
                    Toast.makeText(Register_id_psw_Activity.this,"아이디 중복확인/비밀번호 확인을 완료해주세요",Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnPWCheck = (Button)findViewById(R.id.btnpwcheck);
        btnPWCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(textPW.getText().toString().equals(textPWT.getText().toString()))
                {
                    PWcheck = true;
                    Toast.makeText(Register_id_psw_Activity.this,"비밀번호를 확인완료.",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(Register_id_psw_Activity.this,"비밀번호를 확인해 주세요.",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}

package com.example.seok.tourism.DB_Request;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jung Se In on 2018-02-25.
 */

public class IDcheckRequest extends StringRequest {
    //final static private String URL = "http://203.241.246.193/IDcheck_tour.php";
    final static private String URL = "http://203.241.246.138/Medical/Client/IDcheck_tour.php";
    private Map<String, String> parameters;

    public IDcheckRequest(String user_id,Response.Listener<String> Listener) {
        super(Method.POST, URL, Listener, null);
        parameters = new HashMap<>();
        parameters.put("user_id",user_id);
        Log.d("aaa",""+user_id);
    }

    @Override
    public Map<String, String> getParams(){    return parameters;}
}
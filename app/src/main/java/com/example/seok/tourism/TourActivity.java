package com.example.seok.tourism;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class TourActivity extends AppCompatActivity {

    private EditText textYear, textMonth, textDay, textNum;
    private Button btnOK;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tour);

        textYear = (EditText) findViewById(R.id.textYear);
        textMonth = (EditText) findViewById(R.id.textMonth);
        textDay = (EditText) findViewById(R.id.textDay);
        textNum = (EditText) findViewById(R.id.textNum);

        btnOK = (Button)findViewById(R.id.btnOK);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(textYear.getText().toString().equals("")||textMonth.getText().toString().equals("")||textDay.getText().toString().equals("")||textNum.getText().toString().equals("")) {
                    Toast.makeText(TourActivity.this,"정보란에 공백이 있습니다.",Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(TourActivity.this, "김해 국제공항" + "년, 월, 일" + textYear.getText() + textMonth.getText() + textDay.getText() + "명수" + textNum.getText(), Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(TourActivity.this, ListActivity.class));
                    finish();
                }
            }
        });
    }
}

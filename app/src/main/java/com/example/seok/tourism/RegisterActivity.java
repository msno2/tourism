package com.example.seok.tourism;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.seok.tourism.DB_Request.RegistRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterActivity extends AppCompatActivity {

    private EditText textName, textAge, textGender, textRegion, textPhone;
    private Button btnRegist;
    private String user_id, user_pw,user_name,user_gender,user_adress,user_tel;
    private int user_age;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        textName = (EditText)findViewById(R.id.textName);
        textAge = (EditText)findViewById(R.id.textAge);
        textGender = (EditText)findViewById(R.id.textGender);
        textRegion = (EditText)findViewById(R.id.textRegion);
        textPhone = (EditText)findViewById(R.id.textPhone);

        Intent intent = getIntent();
        user_id = intent.getStringExtra("user_id");
        user_pw = intent.getStringExtra("user_pw");

        btnRegist = (Button)findViewById(R.id.btnRegist);
        btnRegist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(textName.getText().toString().equals("") || textAge.getText().toString().equals("") || textGender.getText().toString().equals("") || textRegion.getText().toString().equals("") || textPhone.getText().toString().equals(""))
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                    builder.setMessage("비어있는 공간이 있습니다. 확인해주세요.")
                            .setNegativeButton("다시 시도", null)
                            .create()
                            .show();
                }
                else
                {
                    user_name = textName.getText().toString();
                    user_age = Integer.parseInt(textAge.getText().toString());
                    user_gender = textGender.getText().toString();
                    user_adress = textRegion.getText().toString();
                    user_tel = textPhone.getText().toString();
                    Response.Listener<String> responseListener = new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                Log.d("aaa", "4");
                                JSONObject jsonResponse = new JSONObject(response);
                                boolean success = jsonResponse.getBoolean("success");
                                Log.d("aaa", success+"");

                                if(success) {
                                    Log.d("aaa", "5");
                                    AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                                    builder.setMessage("회원 등록 성공")
                                            .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                                                    finish();
                                                }
                                            })
                                            .create()
                                            .show();
                                }
                                else
                                {
                                    Log.d("aaa", "6");
                                    AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                                    builder.setMessage("회원 등록 실패")
                                            .setNegativeButton("다시 시도",null)
                                            .create()
                                            .show();
                                }
                            }
                            catch (JSONException e)
                            {
                                Log.d("aaa", "7");
                                e.printStackTrace();
                            }
                        }
                    };

                    RegistRequest registerRequest = new RegistRequest(user_id, user_pw, user_name, user_age,user_gender, user_adress,user_tel,responseListener);
                    RequestQueue queue = Volley.newRequestQueue(RegisterActivity.this);
                    queue.add(registerRequest);
                }
            }
        });

    }
}

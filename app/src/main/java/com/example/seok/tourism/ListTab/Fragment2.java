package com.example.seok.tourism.ListTab;


import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.seok.tourism.ExplainActivity;
import com.example.seok.tourism.ListActivity;
import com.example.seok.tourism.R;

import java.util.ArrayList;

public class Fragment2 extends Fragment implements View.OnClickListener {

    private View view;
    private Spinner spinner;
    private listMenu2 listMenu2;
    private ListView listViewTour;
    private static String str2;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.activity_fragment2, container,false);

        int[] strImg = {R.drawable.testmedical1, R.drawable.testmedical2,R.drawable.testmedical3,R.drawable.testmedical4};
        String[]  strName= {"의료정보1","의료정보2","의료정보3","의료정보4"};
        int nDatCnt = 0;

        ArrayList<listMenu2> arrayList2 = new ArrayList<>();

        for(int i = 0; i<4; i++) {
            listMenu2 = new listMenu2();
            listMenu2.medicalName = strName[nDatCnt];
            //listMenu.btnTour = strPay[nDatCnt];
            listMenu2.mediclaImg = strImg[nDatCnt];
            listMenu2.OnclickListener2 = this;
            nDatCnt++;
            // 디비의 순서대로
            arrayList2.add(listMenu2);
            if (nDatCnt >= strName.length)
                nDatCnt = 0;
        }

        listViewTour = (ListView)view.findViewById(R.id.listview_medical);
        menuAdapter2 adapter2 = new menuAdapter2(arrayList2);
        listViewTour.setAdapter(adapter2);


        spinner = (Spinner)view.findViewById(R.id.spinner_medical);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                //DB작업.
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });



        return view;
    }

    @Override
    public void onClick(View v) {
        int nViewTag = Integer.parseInt((String)v.getTag());
        View oParentView = (View)v.getParent();

        switch (nViewTag)
        {
            case 1:
                String position = (String) oParentView.getTag();

                str2 = "의료 : " + position + "입니다.";
                break;
            case 2:
                Intent intent = new Intent(getActivity(),ExplainActivity.class);
                startActivity(intent);
                break;
        }
    }
}

class menuAdapter2 extends BaseAdapter {
    LayoutInflater inflater = null;
    private ArrayList<listMenu2> m_oData = null;
    private int nListCnt = 0;
    public menuAdapter2(ArrayList<listMenu2> _oData)
    {
        m_oData = _oData;
        nListCnt = m_oData.size();
    }
    @Override
    public int getCount() {
        Log.d("TAG", "getCount");
        return nListCnt;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            final Context context = parent.getContext();
            if (inflater == null) {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }
            convertView = inflater.inflate(R.layout.listview_medical, parent, false);
        }

        LinearLayout medicalLayout = (LinearLayout)convertView.findViewById(R.id.medicalImg);
        TextView textMenuName1 = (TextView) convertView.findViewById(R.id.textview_medicallist);
        textMenuName1.setText(m_oData.get(position).medicalName);

        Button btnMedical = (Button)convertView.findViewById(R.id.btn_medicallist);
        btnMedical.setTag("1");

        medicalLayout.setBackgroundResource(m_oData.get(position).mediclaImg);
        medicalLayout.setTag("2");


        btnMedical.setOnClickListener(m_oData.get(position).OnclickListener2);
        medicalLayout.setOnClickListener(m_oData.get(position).OnclickListener2);

        convertView.setTag(""+position);
        return convertView;
    }
}

class listMenu2{
    public int mediclaImg;
    public String medicalName;
    public View.OnClickListener OnclickListener2;
}
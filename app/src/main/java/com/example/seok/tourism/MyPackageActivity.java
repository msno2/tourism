package com.example.seok.tourism;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.seok.tourism.DB_Request.ConsultingRequest;
import com.example.seok.tourism.DB_Request.ConsultingRequest_MyPackage;

import org.json.JSONObject;

public class MyPackageActivity extends AppCompatActivity {

    private Button btnOK;
    String user_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_package);

        user_id = UserInfomation.getInstance().getUserId().toString();


        btnOK = (Button)findViewById(R.id.btnok);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Response.Listener<String> responListener = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try
                        {
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");
                            if(success)
                            {
                                AlertDialog.Builder builder = new AlertDialog.Builder(MyPackageActivity.this);
                                builder.setMessage("예약 완료")
                                        .setPositiveButton("예약완료", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                                finish();
                                            }
                                        })
                                        .create()
                                        .show();
                            }
                            else
                            {
                                AlertDialog.Builder builder = new AlertDialog.Builder(MyPackageActivity.this);
                                builder.setMessage("오류 발생")
                                        .setNegativeButton("오류 발생", null)
                                        .create()
                                        .show();
                            }
                        } catch (Exception e) { e.printStackTrace(); }
                    }
                };

                ConsultingRequest_MyPackage consultingrequest_mypackage = new ConsultingRequest_MyPackage(user_id, responListener);
                RequestQueue queue = Volley.newRequestQueue(MyPackageActivity.this);
                queue.add(consultingrequest_mypackage);
            }
        });

    }
}

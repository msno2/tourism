package com.example.seok.tourism;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.ImageView;

public class LauncherActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);

        mHandler.sendEmptyMessageDelayed(0,2000);
    }
    private Handler mHandler =new Handler(){
        @Override
        public void handleMessage(Message msg){
            super.handleMessage(msg);
            startActivity(new Intent(LauncherActivity.this,LoginActivity.class));
            LauncherActivity.this.finish();
        }
    };
}

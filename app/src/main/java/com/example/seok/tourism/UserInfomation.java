package com.example.seok.tourism;

import android.util.Log;

/**
 * Created by Seok on 2018-02-27.
 */
public class UserInfomation {

    private String userId;
    private String userName;
    private String userTel;
    private String userAge;
    private String userGender;
    private String userAddress;
    private String userConsulting;

    private static UserInfomation instance = new UserInfomation();
    private  UserInfomation(){

    }
    public static UserInfomation getInstance(){
        //없을시 객체 생성 ( 만약 오류로 객체 생성 못켰을때 )
        if ( instance == null )
            instance = new UserInfomation();
        return instance;
    }
    public void setUserId(String userId){
        this.userId = userId;
    }
    public String getUserId(){
        return this.userId;
    }
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getUserTel() {
        return userTel;
    }
    public void setUserTel(String userTel) {
        this.userTel = userTel;
    }
    public String getUserAge() {
        return userAge;
    }
    public void setUserAge(String userAge) {
        this.userAge = userAge;
    }
    public String getUserAddress() {
        return userAddress;
    }
    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }
    public String getUserConsulting() {
        return userConsulting;
    }
    public void setUserConsulting(String userConsulting) {
        this.userConsulting = userConsulting;
    }
    public String getUserGender() {
        return userGender;
    }
    public void setUserGender(String userGender) {
        this.userGender = userGender;
    }
    public static void setInstance(UserInfomation instance) {
        UserInfomation.instance = instance;
    }

}
